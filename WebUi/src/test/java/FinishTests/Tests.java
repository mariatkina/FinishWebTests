package FinishTests;

import FinishProject.AuthPage;
import FinishProject.MyPostsPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Tests {
    WebDriverWait webDriverWait;
//    public String hello = "//a[.='Hello,%']";
    static Properties properties = new Properties();
    private static String baseUrl;
    private static String username;
    private static String password;
    private static String token;
    private static InputStream configFile;
    static WebDriver driver;
    static AuthPage authPage;
     MyPostsPage myPostsPage;
     //NotMyPostsPage notMyPostsPage;
    public static String getBaseUrl() {
        return baseUrl;
    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static String getToken() {
        return token;
    }

    @BeforeAll
    static void beforeTest() throws IOException {
        configFile = new FileInputStream("src/main/java/resources/my.properties");
        properties.load(configFile);

        baseUrl = properties.getProperty("baseUrl");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        token = properties.getProperty("token");

        WebDriverManager.chromedriver().setup();
    }
    @BeforeEach
    void openSession(){
        driver = new ChromeDriver();
        driver.get(getBaseUrl());
    }

    @Order(1)
    @Test
    @DisplayName("1.авторизация с валидными логином и паролем")
    void authSuccess() {
        authPage = new AuthPage(driver);
        authPage.authorization(getUsername(), getPassword());
        myPostsPage = new MyPostsPage(driver);
        myPostsPage.assertRegister();
    }
    @Test
    @Order(2)
    @DisplayName("2.проверка ввода пустого поля логина")
    void authWithoutLogin() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData("", getPassword());
    }
    @Test
    @Order(3)
    @DisplayName("3.проверка ошибки в логине")
    void authWithInvalidLogin1() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData("aok", getPassword());
    }
    @Test
    @Order(4)
    @DisplayName("4.проверка невалидного написания логина не латиницей")
    void authWithInvalidLogin2() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData("эдик", "f32b74652c");
    }
    @Test
    @Order(5)
    @DisplayName("5.проверка ввода в поле логина более 20 символов(21)")
    void authWithInvalidLogin3() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData("Abcdefghigklmnoprstuv", "a91b48d559");
    }
    @Test
    @Order(6)
    @DisplayName("6.проверка ввода в поле логина менее 3 символов(2)")
    void authWithInvalidLogin4() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData("w1", "a95dcb8aeb");
    }
    @Test
    @Order(7)
    @DisplayName("7.проверка ввода неправильного пароля")
    void authWithInvalidPass1() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData(getUsername(), "a95dcb8aeb");
    }
    @Test
    @Order(8)
    @DisplayName("8.проверка авторизации без ввода пароля")
    void authWithInvalidPass2() {
        authPage = new AuthPage(driver);
        authPage.invalidAuthData(getUsername(), "");
    }
    @Test
    @Order(9)
    @DisplayName("9.проверка работы кнопки сортировки постов пользователя")
    void myPostsPageTest1() {
        authPage = new AuthPage(driver);
        authPage.authorization(getUsername(), getPassword());
        myPostsPage = new MyPostsPage(driver);
        myPostsPage.myPostsPageAddr();
    }
    @Test
    @Order(10)
    @DisplayName("10.проверка видимости в ленте своих постов картинки, заголовка и аннотации к посту")
    void myPostsPicTest1() {
        authPage = new AuthPage(driver);
        authPage.authorization(getUsername(), getPassword());
        myPostsPage = new MyPostsPage(driver);
        myPostsPage.picture();
    }
    @Test
    @Order(11)
    @DisplayName("11.проверка переключения на следующую страницу")
    void changePageTest() throws InterruptedException {
        authPage = new AuthPage(driver);
        authPage.authorization(getUsername(), getPassword());
        myPostsPage = new MyPostsPage(driver);
        myPostsPage.nextPage();
    }

    @AfterEach
    void tearDown() {
        LogEntries browserLogs = driver.manage().logs().get(LogType.BROWSER);
        List<LogEntry> allLogRows = browserLogs.getAll();
        if (allLogRows.size() > 0 ) {
            allLogRows.forEach(logEntry -> {
                System.out.println(logEntry.getMessage());
            });
            driver.quit();
        }
        driver.quit();
    }
}
