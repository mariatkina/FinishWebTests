package FinishProject;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AuthPage extends BaseView {
    MyPostsPage myPostsPage;


    public AuthPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//input[@type='text']")
    public WebElement userLogin;
    @FindBy(xpath = "//input[@type='password']")
    public WebElement userPasswrd;
    @FindBy(xpath = "//button")
    public WebElement loginButtn;
    @FindBy(xpath = "//h2[.='401']")
    public WebElement message;



    public MyPostsPage authorization(String login, String password){
        userLogin.sendKeys(login);
        userPasswrd.sendKeys(password);
        loginButtn.click();
        return new MyPostsPage(driver);
    }
    public void invalidAuthData(String login, String password){
        userLogin.sendKeys(login);
        userPasswrd.sendKeys(password);
        loginButtn.click();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[.='401']")));

        Assertions.assertTrue(message.isDisplayed());
    }

}
