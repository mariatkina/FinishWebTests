package FinishProject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderMenu extends BaseView{
    public HeaderMenu(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//span[.='Home']")
    public WebElement home;
    @FindBy(xpath = "//a[.='About']")
    public WebElement about;
    @FindBy(xpath = "//a[.='Contact']")
    public WebElement contact;
    @FindBy(xpath = "//a[.='Hello, ']")
    public WebElement helloUser;

}
