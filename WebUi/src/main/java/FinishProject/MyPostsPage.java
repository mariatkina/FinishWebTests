package FinishProject;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MyPostsPage extends BaseView{
    AuthPage authPage;
    public MyPostsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//span[.='Home']")
    public WebElement home;
    @FindBy(xpath = "//a[.='About']")
    public WebElement about;
    @FindBy(xpath = "//a[.='Contact']")
    public WebElement contact;
    @FindBy(xpath = "//a[.='Hello,%']")
    public WebElement helloUser;
    @FindBy(xpath = "//h1[.='Blog']")
    public WebElement blog;
    @FindBy(xpath = "//main[@class = 'svelte-1pbgeyl']")
    public WebElement mainFrame;
    @FindBy(xpath = "//button[@aria-pressed = 'false']")
    public WebElement sortButtonOff;
    @FindBy(xpath = "//button[@aria-pressed = 'false']")
    public WebElement sortButtonOn;
    @FindBy(xpath = "//div[@class='posts svelte-127jg4t']//a[1]")
    public WebElement firstPicture;
    @FindBy(xpath = "//div[@class='posts svelte-127jg4t']//a[1]//h2[@class='svelte-127jg4t']")
    public WebElement title;
    @FindBy(xpath = "//div[@class='posts svelte-127jg4t']//a[1]//div[@class='description svelte-127jg4t']")
    public WebElement description;

    public void assertRegister(){
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[.='About']")));

        Assertions.assertTrue(driver.findElement(By.id("create-btn")).isDisplayed());
        Assertions.assertTrue(about.isDisplayed());
    }
    public void myPostsPageAddr(){
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-pressed = 'false']")));
        actions.moveToElement(sortButtonOff).click().perform();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-pressed = 'true']")));
        driver.findElement(By.xpath("//button[@aria-pressed = 'true']")).click();

        Assertions.assertTrue(driver.getCurrentUrl().contains("?sort=createdAt&order=ASC"));
    }
    public void picture(){
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Home']")));
        Assertions.assertTrue(firstPicture.isDisplayed());
        System.out.println("Присутствует картинка поста");
        Assertions.assertTrue(title.isDisplayed());
        System.out.println("Присутствует загловок поста");
        Assertions.assertTrue(description.isDisplayed());
        System.out.println("Присутствует описание поста");
    }
    public void nextPage() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Home']")));
        driver.findElement(By.xpath("//a[. = 'Next Page']")).click();
        Thread.sleep(5000);
        Assertions.assertTrue(driver.getCurrentUrl().contains("?page=2"));
    }

}
